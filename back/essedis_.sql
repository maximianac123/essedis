-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Май 10 2023 г., 19:44
-- Версия сервера: 10.4.27-MariaDB
-- Версия PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `essedis`
--

-- --------------------------------------------------------

--
-- Структура таблицы `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `post` varchar(50) NOT NULL,
  `image` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `about`
--

INSERT INTO `about` (`id`, `name`, `post`, `image`) VALUES
(18, 'Дмитрий Терзинов', 'Председатель ассоциации', '1682278826_dimon4ik.png'),
(19, 'Николай Дишли', 'Секретарь Совета ассоциации', '1682091293_Nicolai.png'),
(20, 'Сергей Галаслы', 'Член ревизионной комиссии', '1682091419_Serghei.png'),
(21, 'Анастасия Янак', 'Волонтёр', '1682091519_Nastiuha.png');

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `adminName` varchar(50) NOT NULL,
  `password` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`id`, `adminName`, `password`) VALUES
(14, 'Maxim', '$2y$10$wtL2xx7DTcezZymKhs8ykOdDcE8.EmgynllntjNSUK2W.n0f7asrC');

-- --------------------------------------------------------

--
-- Структура таблицы `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `gallery`
--

INSERT INTO `gallery` (`id`, `name`, `image`) VALUES
(13, 'Победители конкурса школьных рисунков \"С чего начинается Родина?\"', '1682618444_Image 21@2x.png');

-- --------------------------------------------------------

--
-- Структура таблицы `galleryimages`
--

CREATE TABLE `galleryimages` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `imagePaths` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `galleryimages`
--

INSERT INTO `galleryimages` (`id`, `gallery_id`, `imagePaths`) VALUES
(13, 13, '1682618444_Image 21@2x.png,1682618444_Image 22@2x.png,1682618444_Image 23@2x.png,1682618444_Image 24@2x.png,1682618444_Image 25@2x.png,1682618444_Image 27@2x.png,1682618444_Image 28@2x.png,1682618444_Image 29@2x.png,1682618444_Image 30@2x.png,1682618444_Image 31@2x.png,1682618444_Image 32@2x.png');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id_post` int(10) NOT NULL,
  `postBody` text NOT NULL,
  `postImages` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id_post`, `postBody`, `postImages`, `date`) VALUES
(145, '<h3>Творческий онлайн-конкурс \"Моя семья\"</h3>\r\n<p><br></p>\r\n<p>Ребятки, привет! Вы соскучились по нам? Вот мы по вам очень соскучились и решили, что пора провести конкурс!&nbsp;</p>\r\n<p><br></p>\r\n<p>Ко дню Семьи, любви и верности НПО \"Pharos \" совместно с Представительством Россотрудничества в Республике Молдова и НПО \"Essedis\" объявляют творческий онлайн-конкурс \"Моя Семья\" !&nbsp;</p>\r\n<p><br></p>\r\n<p>Участниками конкурса могут стать представители любого возраста и из любой точки Гагаузии!&nbsp;</p>\r\n<p><br></p>\r\n<p>Конкурс имеет 3 номинации:&nbsp;</p>\r\n<p>- Изобразительное искусство(рисунок)&nbsp;</p>\r\n<p>- Поэзия (рассказать стихотворение, не обязательно собственного произведения, но приветствуется)&nbsp;</p>\r\n<p>- Музыкальное произведение (песня)&nbsp;</p>\r\n<p><br></p>\r\n<p>Для участия в конкурсе:&nbsp;</p>\r\n<p>- выложить пост в инстаграме на соответствующую тему (семья, любовь, верность)&nbsp;</p>\r\n<p>- отметить аккаунты @ngo_pharos, @centrulrusmoldova и @ngo.essedis&nbsp;</p>\r\n<p>- отметить специальными хэштегами #нпо_Фарос #нпо_Есседис #россотрудничество #россотрудничество_молдова #МолдоваРЦНК #Россотрудничество_Гагаузия&nbsp;</p>\r\n<p><br></p>\r\n<p>Конкурс продлится до 21.07.2020&nbsp;</p>\r\n<p><br></p>\r\n<p>Победителей ждут ценные призы!!!&nbsp;</p>\r\n<p><br></p>\r\n<p>P.S. Участник может подавать заявки во всех номинациях, но выиграть только в одной!</p>\r\n<p><br>\r\n<br>\r\n</p>', '1682278058_Image.png', '2023-04-23'),
(146, '<h3>Международный интеграционный форум \"Приграничье-2019\"</h3>\r\n<p><br></p>\r\n<p>10 -11 декабря 2019 года в г. Белгороде состоялся Международный интеграционный форум «Приграничье-2019».&nbsp;</p>\r\n<p>Организаторами форума выступили Институт приграничного сотрудничества и интеграции совместно с Белгородским государственным национальным исследовательским университетом при поддержке Межгосударственного фонда гуманитарного сотрудничества государств-участников СНГ.&nbsp;</p>\r\n<p><br></p>\r\n<p>В рамках мероприятия состоялись дискуссии, круглые столы и панельные секции по актуальным вопросам развития международной интеграции государств-участников СНГ, реализации проектов общественной дипломатии, приграничного сотрудничества и межрегионального взаимодействия.&nbsp;</p>\r\n<p><br></p>\r\n<p>В Форуме приняло участие более 60 экспертов из различных регионов России, Молдовы, Беларуси, Казахстана, Армении, Украины, Азербайджана и др. стран&nbsp;</p>\r\n<p><br></p>\r\n<p>В мероприятиях форума приняло участие и Общественная Ассоциация \"Essedis\".</p>', '1682278256_Image 2.png', '2023-04-23');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `galleryimages`
--
ALTER TABLE `galleryimages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gallery_id` (`gallery_id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id_post`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `galleryimages`
--
ALTER TABLE `galleryimages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id_post` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `galleryimages`
--
ALTER TABLE `galleryimages`
  ADD CONSTRAINT `galleryimages_ibfk_1` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
