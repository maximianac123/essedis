<?php
    header('Access-Control-Allow-Origin: *');
    
    $mysqli = mysqli_connect("localhost", "root", "", "essedis");

    if (!$mysqli) {
        die("Connection failed");
    };

    $data = mysqli_query($mysqli, "SELECT * from news");
    $items = array();

    while ($dat = mysqli_fetch_array($data)) {
        $items[] = array(
            "id" => $dat[0],
            "images" => explode(",", $dat[2]),
            "newsInfo" => $dat[1],
            "publicDate" => $dat[3]
        );
    }

    echo json_encode(array("items" => $items));

    mysqli_close($mysqli);
?>