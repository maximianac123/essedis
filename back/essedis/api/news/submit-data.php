<?php
    header('Access-Control-Allow-Origin: *');

    $mysqli = mysqli_connect("localhost", "root", "", "essedis");

    if (!$mysqli) {
        die("Connection failed");
    };

    // Получаем файл
    if (isset($_FILES) && isset($_POST['htmlText'])) {
        $images = $_FILES["images"];
        $normalizeImages = [];

        foreach ($images as $key_name => $value) {
            foreach ($value as $key => $item) {
                $normalizeImages[$key][$key_name] = $item;
            }
        }

        $uploadPaths = array();

        foreach ($normalizeImages as $image) {
            $extension = pathinfo($image['name'], PATHINFO_EXTENSION);
            $filename = time() . "_" . basename($image['name'], ".$extension") . ".$extension";

            // Загружаем файл на сервер и получаем новое имя файла
            $uploadPath = '../../upload/news-image/' . $filename;
            move_uploaded_file($image['tmp_name'], $uploadPath);

            array_push($uploadPaths, $filename);
        }
        $postImages = implode(",", $uploadPaths);
        $htmlText = $_POST['htmlText'];


        mysqli_query($mysqli, "INSERT INTO news (id_post, postBody, postImages, date) VALUES (NULL, '$htmlText', '$postImages', NOW())");
    }


    mysqli_close($mysqli);
?>