<?php
    header('Access-Control-Allow-Origin: *');
    
    $mysqli = mysqli_connect("localhost", "root", "", "essedis");

    if (!$mysqli) {
        die("Connection failed");
    };

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        
        $data = mysqli_query($mysqli, "SELECT postBody FROM news WHERE id_post = $id");
        $htmlArray = mysqli_fetch_assoc($data);
        $html = $htmlArray['postBody'];

        echo "$html";
    }

    if (isset($_POST['id'])) {
        if (isset($_FILES['images'])) {
            $id = $_POST['id'];
            $images = $_FILES["images"];
            $htmlText = $_POST['htmlText'];

            $normalizeImages = [];

            foreach ($images as $key_name => $value) {
                foreach ($value as $key => $item) {
                    $normalizeImages[$key][$key_name] = $item;
                }
            }

            $uploadPaths = array();

            foreach ($normalizeImages as $image) {
                $extension = pathinfo($image['name'], PATHINFO_EXTENSION);
                $filename = time() . "_" . basename($image['name'], ".$extension") . ".$extension";

                // Загружаем файл на сервер и получаем новое имя файла
                $uploadPath = '../../upload/news-image/' . $filename;
                move_uploaded_file($image['tmp_name'], $uploadPath);

                array_push($uploadPaths, $filename);
            }
            $postImages = implode(",", $uploadPaths);



            $dataOfImages = mysqli_query($mysqli, "SELECT postImages FROM news WHERE id_post = $id");

            $row = mysqli_fetch_assoc($dataOfImages);
            $fileString = $row['postImages'];
            
            $fileArr = explode(',', $fileString);

            foreach ($fileArr as $filename) {
                $fTrimmed = trim($filename);
                $filepath = "../../upload/news-image/" . $fTrimmed;
                if (file_exists($filepath)) {
                    unlink($filepath);
                }
            }

            mysqli_query($mysqli, "UPDATE `news` SET `postBody` = '$htmlText', `postImages` = '$postImages' WHERE `news`.`id_post` = $id");
        } else {
            $id = $_POST['id'];
            $htmlText = $_POST['htmlText'];

            mysqli_query($mysqli, "UPDATE `news` SET `postBody` = '$htmlText' WHERE `news`.`id_post` = $id");
        }
    }

    mysqli_close($mysqli);

?>