<?php
    header('Access-Control-Allow-Origin: *');
    
    $mysqli = mysqli_connect("localhost", "root", "", "essedis");

    if (!$mysqli) {
        die("Connection failed");
    };


    if (isset($_GET['id'])) {
        $id = $_GET['id'];

        $data = mysqli_query($mysqli, "SELECT postImages FROM news WHERE id_post = $id");

        mysqli_query($mysqli, "DELETE FROM news WHERE id_post = $id");

        $row = mysqli_fetch_assoc($data);
        $fileString = $row['postImages'];
        
        $fileArr = explode(',', $fileString);

        foreach ($fileArr as $filename) {
            $fTrimmed = trim($filename);
            $filepath = "../../upload/news-image/" . $fTrimmed;
            if (file_exists($filepath)) {
                unlink($filepath);
            }
        }
    };

    mysqli_close($mysqli);
?>