<?php
    header('Access-Control-Allow-Origin: *');
    
    $mysqli = mysqli_connect("localhost", "root", "", "essedis");

    if (!$mysqli) {
        die("Connection failed");
    };

    $data = mysqli_query($mysqli, "SELECT * from gallery");
    $items = array();

    while ($dat = mysqli_fetch_array($data)) {
        $items[] = array(
            "id" => $dat[0],
            "name" => $dat[1],
            "image" => $dat[2],
        );
    }

    echo json_encode(array("items" => $items));

    mysqli_close($mysqli);

?>