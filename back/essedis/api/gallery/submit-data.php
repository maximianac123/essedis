<?php
    header('Access-Control-Allow-Origin: *');
    
    $mysqli = mysqli_connect("localhost", "root", "", "essedis");

    if (!$mysqli) {
        die("Connection failed");
    };

    if (isset($_FILES) && isset($_POST['name'])) {
        $coverImage = $_FILES['cover'];
        $itemImages = $_FILES['file'];
        $name = $_POST['name'];


        $extensionCover = pathinfo($coverImage['name'], PATHINFO_EXTENSION);
        $filenameCover = time() . "_" . basename($coverImage['name'], ".$extensionCover") . ".$extensionCover";

        $uploadPath = '../../upload/gallery/galleryMain/' . $filenameCover;
        move_uploaded_file($coverImage['tmp_name'], $uploadPath);

        mysqli_query($mysqli, "INSERT INTO gallery (id, name, image) VALUES (NULL, '$name', '$filenameCover')");

        $normalizeImages = [];
        foreach ($itemImages as $key_name => $value) {
            foreach ($value as $key => $item) {
                $normalizeImages[$key][$key_name] = $item;
            }
        }

        $uploadPaths = array();
        $galleryId = mysqli_insert_id($mysqli);
        foreach ($normalizeImages as $image) {
            $extension = pathinfo($image['name'], PATHINFO_EXTENSION);
            $filename = time() . "_" . basename($image['name'], ".$extension") . ".$extension";

            $galleryFolder = '../../upload/gallery/galleryItems/' . $galleryId;
            if (!file_exists($galleryFolder)) {
                mkdir($galleryFolder, 0777, true);
            }

            // Загружаем файл на сервер и получаем новое имя файла
            $uploadPath = $galleryFolder . '/' . $filename;
            move_uploaded_file($image['tmp_name'], $uploadPath);

            array_push($uploadPaths, $filename);
        }
        $imagePaths = implode(",", $uploadPaths);


        mysqli_query($mysqli, "INSERT INTO galleryimages (id, gallery_id, imagePaths) VALUES (NULL, $galleryId, '$imagePaths')");
    }
    mysqli_close($mysqli);
?>
