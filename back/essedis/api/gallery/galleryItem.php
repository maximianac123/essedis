<?php
    header('Access-Control-Allow-Origin: *');
    
    $mysqli = mysqli_connect("localhost", "root", "", "essedis");

    if (!$mysqli) {
        die("Connection failed");
    };

    if (isset($_GET['id'])) {
        $id = $_GET['id'];

        $data = mysqli_query($mysqli, "SELECT imagePaths FROM galleryimages WHERE gallery_id = $id");

        $items = array();

        while ($dat = mysqli_fetch_array($data)) {
            $items[] = array(
                "imagePaths" => explode(",", $dat[0]),
            );
        }

    }

    echo json_encode(array("items" => $items));

    mysqli_close($mysqli);


?>