<?php
    header('Access-Control-Allow-Origin: *');
    
    $mysqli = mysqli_connect("localhost", "root", "", "essedis");

    if (!$mysqli) {
        die("Connection failed");
    };

    if (isset($_GET['id'])) {
        $id = $_GET['id'];

        $imageCover = mysqli_query($mysqli, "SELECT image FROM gallery WHERE id = $id");

        // $itemImages = mysqli_query($mysqli, "SELECT imagePaths FROM galleryimages WHERE gallery_id = $id");

        $imagePath = mysqli_fetch_assoc($imageCover);
        $fTrimmed = trim($imagePath['image']);
        $filepath = "../../upload/gallery/galleryMain/" . $fTrimmed;
        if (file_exists($filepath)) {
            unlink($filepath);
        }

        function rmdirr($dirPath) {
            if (!is_dir($dirPath)) {
                return;
            }
        
            $files = scandir($dirPath);
            foreach ($files as $file) {
                if ($file === '.' || $file === '..') {
                    continue;
                }
        
                $path = $dirPath . '/' . $file;
                if (is_dir($path)) {
                    rmdirr($path);
                } else {
                    unlink($path);
                }
            }
        
            rmdir($dirPath);
        }

        $dirPath = "../../upload/gallery/galleryItems/" . $id;

        if (is_dir($dirPath)) {
            rmdirr($dirPath);
        }

        mysqli_query($mysqli, "DELETE FROM galleryimages WHERE gallery_id = $id");
        mysqli_query($mysqli, "DELETE FROM gallery WHERE id = $id");
    };

    mysqli_close($mysqli);
?>