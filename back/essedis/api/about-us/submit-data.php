<?php
    header('Access-Control-Allow-Origin: *');
    
    $mysqli = mysqli_connect("localhost", "root", "", "essedis");

    if (!$mysqli) {
        die("Connection failed");
    };

    if (isset($_FILES) && isset($_POST['name']) && isset($_POST['position'])) {
        $image = $_FILES['file'];
        $name = $_POST['name'];
        $position = $_POST['position'];

        $extension = pathinfo($image['name'], PATHINFO_EXTENSION);
        $filename = time() . "_" . basename($image['name'], ".$extension") . ".$extension";

        $uploadPath = '../../upload/about-us-image/' . $filename;
        move_uploaded_file($image['tmp_name'], $uploadPath);

        mysqli_query($mysqli, "INSERT INTO about (id, name, post, image) VALUES (NULL, '$name', '$position', '$filename')");

    }

    mysqli_close($mysqli);
?>