<?php
    header('Access-Control-Allow-Origin: *');

    session_start();

    $params = session_get_cookie_params();
    $lifetime = 86400;

    setcookie(session_name(), session_id(), time() + $lifetime, $params['path'], $params['domain'], false, true);

    $_SESSION['isAdmin'] = true;
?>