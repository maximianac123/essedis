<?php
    header('Access-Control-Allow-Origin: *');
    
    $mysqli = mysqli_connect("localhost", "root", "", "essedis");

    if (!$mysqli) {
        die("Connection failed");
    };

    if(isset($_POST['username']) && isset($_POST['password'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
    
        $username = mysqli_real_escape_string($mysqli, $username);
        $password = mysqli_real_escape_string($mysqli, $password);

        $result = mysqli_query($mysqli, "SELECT password FROM admin WHERE adminName ='$username'");
        $row = mysqli_fetch_assoc($result);
        $hash = $row['password'];

        if (password_verify($password, $hash)) {
            echo "true";
            require "admin-session.php";
        } else {
            echo "false";
        }

        mysqli_close($mysqli);
    }

?>